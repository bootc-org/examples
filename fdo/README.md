# Setup FDO

This example installs the FDO tools required to run FDO with Anaconda
and onboard the device on first boot. It requires a full FDO stack to be
deployed and running already.

Since running FDO needs an installer, this example requires Anaconda (built
using bootc-image-builder). An example kickstart is also provided within
the bootc-image-builder's configuration file.

