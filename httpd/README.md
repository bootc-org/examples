# HTTPD

This example provides an Apache HTTPD server exposed on port 80.

## Building this example

1. Build the image with either `podman build` or [Podman Desktop](https://podman-desktop.io/).
2. (Optional for login and SSH access) Create a  [`config.toml` or `config.json` build config](https://docs.fedoraproject.org/en-US/bootc/authentication/#_bootc_image_builder) that contains login and SSH information.
3. Build the image with either [bootc-image-builder](https://github.com/osbuild/bootc-image-builder) or the [Podman Desktop BootC extension](https://github.com/containers/podman-desktop-extension-bootc) and (optionally) the `config.toml` you created.

## Using this example

1. Launch the virtual machine.
2. Visit the VM IP address `http://<ip-address>` on your browser.