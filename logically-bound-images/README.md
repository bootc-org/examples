This demonstrates "logically bound images"; for background see
https://containers.github.io/bootc/experimental-logically-bound-images.html
 
In this case we set up the [Prometheus node exporter](https://github.com/prometheus/node_exporter)
as well as a demo webserver.

