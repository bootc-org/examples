# Example containers for the Fedora/CentOS bootc project

This repository contains example community-maintained container build examples for
the [Fedora/CentOS bootc project](https://docs.fedoraproject.org/en-US/bootc/).

## Using these examples

These are just references intended to be used as a starting point.
For a few images, such as `cloud-init` it is possible that simply building the
container image exactly from this example results in a sufficient system,
but the overall idea is that you merge some of these examples with much
more nontrivial configuration for your desired system.

For example, it may be common to combine one of the clouds/hypervisors example code
alongside with embedded configuration for running workload containers.

TIP: Many of these examples reference CentOS Stream 9.
You can use e.g. `podman build --from` to override the `FROM` line to use a Fedora
image, or another base image of your choosing.

## Pulling these examples

Some examples are published to an image registry and are set up within [our CI](https://gitlab.com/fedora/bootc/examples/-/blob/main/.gitlab-ci.yml?ref_type=heads).

These images are for the **AMD64** architecture.

For a list of images available for pulling, see the [GitLab Container Registry](https://gitlab.com/fedora/bootc/examples/container_registry).

You can pull these images using:

```sh
$ podman pull registry.gitlab.com/fedora/bootc/examples/httpd
```

## SSH'ing into these examples

When building these images, **it is not recommended** to inject your public SSH key directly.

Instead, when using [bootc-image-builder](https://github.com/osbuild/bootc-image-builder), create a [`config.toml` or `config.json`](https://docs.fedoraproject.org/en-US/bootc/authentication/#_bootc_image_builder) build config file.

This configuration allows you to add your SSH key automatically as shown regardless of what image you choose to build:

```toml
[[customizations.user]]
name = "alice"
password = "bob"
key = "ssh-rsa AAA ... user@email.com"
groups = ["wheel"]
```

## List of examples

### Clouds/hypervisors

- [cloud-init](cloud-init/): Image to be used with cloud-init-capable systems (e.g. AWS, KubeVirt, etc.)
- [gcp](gcp/): Install the Google Compute Engine guest agent
- [vmware](vmware/): Install open-vm-tools for use with VMware
- [qemu-guest-agent](qemu-guest-agent): Install the qemu-guest-agent

### OS configuration

- [container-auth](container-auth/): Inject a unified container pull secret
- [transient-etc](transient-etc/): Configure `/etc` to be a transient overlayfs
- [ansible-firewalld](ansible-firewalld/): Example of running an Ansible playbook at build time
- [wifi](wifi/): Install support for wireless networks along with pre-baked
  configuration to join a network
- [httpd](httpd/): Run an apache webserver
- [docker](docker/): Install docker-ce
- [insights](insights/): Register with Red Hat insights
- [app-podman-systemd](embed-workloads/): Run an application container via included config
- [embed-workloads](embed-workloads/): Run application containers via included config
- [tailscale](tailscale/): Install the <https://tailscale.com/> agent
- [microshift](microshift/): Run MicroShift: a lightweight K8s distribution for the edge
- [nvidia](nvidia/): Install the nvidia driver
- [kargs](kargs/): Configure day 0 and day 2 kernel arguments

## Other useful repositories

- A lot of content from our friends at [Universal Blue](https://universal-blue.org); see in particular [github.com/ublue-os](https://github.com/ublue-os/)
- <https://github.com/coreos/layering-examples>
- <https://github.com/openshift/rhcos-image-layering-examples/>
